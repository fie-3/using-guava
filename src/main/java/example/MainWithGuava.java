package example;

// La bibliothèque Guava fournit (entre autres) la classe 'Multi-Ensemble'
// cf. https://github.com/google/guava xxx
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
 

public class MainWithGuava {
    
    public static void main(String... args) {
        // Un multi-ensemble peut contenir plusieurs fois le même élément
        Multiset<String> books = HashMultiset.create();
        
        books.add("Harry Potter");
        books.add("Lord of the Rings");
        // On ajoute plusieurs fois le même livre
        books.add("Lord of the Rings");
        books.add("Harry Potter");
        books.add("Harry Potter");
        books.add("Harry Potter");
        
        // printf(...) cf. https://www.baeldung.com/java-printstream-printf
        System.out.printf("On a %d exemplaires de Harry Potter %n", books.count("Harry Potter"));
        System.out.printf("On a %d exemplaires de Lord of the Rings %n", books.count("Lord of the Rings"));
        
    }
    
}
